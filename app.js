import express from "express";
import cors from "cors";
import dotenv from 'dotenv';
import passport from 'passport';
import jwtStrategy from './src/auth/strategy.js';
import helmet from "helmet";

import { countriesRouter } from "./src/controllers/countries-controller.js";
import { usersRouter } from "./src/controllers/users-controller.js";
import { shipmentsRouter } from "./src/controllers/shipments-controller.js";
import { parcelsRouter } from "./src/controllers/parcels-controller.js";
import { warehousesRouter } from "./src/controllers/warehouses-controller.js";
import { categoriesRouter } from "./src/controllers/categories-controller.js";
import swaggerUI from "swagger-ui-express";

const constants = dotenv.config().parsed;
const PORT = parseInt(constants.PORT);

passport.use(jwtStrategy);

const app = express();

app.use(cors(), helmet(), express.json());

app.use("/users", usersRouter);
app.use("/countries", countriesRouter);
app.use("/shipments", shipmentsRouter);
app.use("/parcels", parcelsRouter);
app.use("/warehouses", warehousesRouter);
app.use("/categories", categoriesRouter);

app.listen(PORT, () => console.log(`Listening on PORT: ${PORT}`));
