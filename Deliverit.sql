-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema deliver
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema deliver
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `deliver` DEFAULT CHARACTER SET latin1 ;
USE `deliver` ;

-- -----------------------------------------------------
-- Table `deliver`.`categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `deliver`.`categories` (
  `category` VARCHAR(45) NOT NULL,
  `is_deleted` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`category`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `deliver`.`countries`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `deliver`.`countries` (
  `country` VARCHAR(45) NOT NULL,
  `is_deleted` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`country`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `deliver`.`cities`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `deliver`.`cities` (
  `city` VARCHAR(45) NOT NULL,
  `country` VARCHAR(45) NOT NULL,
  `is_deleted` VARCHAR(45) NOT NULL DEFAULT '0',
  PRIMARY KEY (`city`),
  INDEX `fk_cities_countries1_idx` (`country` ASC),
  CONSTRAINT `fk_cities_countries1`
    FOREIGN KEY (`country`)
    REFERENCES `deliver`.`countries` (`country`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `deliver`.`status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `deliver`.`status` (
  `status` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`status`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `deliver`.`warehouses`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `deliver`.`warehouses` (
  `idwarehouses` INT(11) NOT NULL AUTO_INCREMENT,
  `city` VARCHAR(45) NOT NULL,
  `country` VARCHAR(45) NOT NULL,
  `street` VARCHAR(45) NOT NULL,
  `is_deleted` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`idwarehouses`),
  INDEX `fk_warehouses_cities1_idx` (`city` ASC),
  INDEX `fk_warehouses_countries1_idx` (`country` ASC),
  CONSTRAINT `fk_warehouses_cities1`
    FOREIGN KEY (`city`)
    REFERENCES `deliver`.`cities` (`city`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_warehouses_countries1`
    FOREIGN KEY (`country`)
    REFERENCES `deliver`.`countries` (`country`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 36
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `deliver`.`shipments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `deliver`.`shipments` (
  `idshipments` INT(11) NOT NULL AUTO_INCREMENT,
  `status` VARCHAR(45) NOT NULL DEFAULT 'preparing',
  `from_idwarehouses` INT(11) NOT NULL,
  `to_idwarehouses` INT(11) NOT NULL,
  `departure` DATETIME NULL DEFAULT NULL,
  `arrival` DATETIME NULL DEFAULT NULL,
  `created_on` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  `is_deleted` TINYINT(1) NOT NULL DEFAULT 0,
  `shipment_weight` VARCHAR(45) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idshipments`, `status`),
  INDEX `fk_shipments_warehouses1_idx` (`from_idwarehouses` ASC),
  INDEX `fk_shipments_warehouses2_idx` (`to_idwarehouses` ASC),
  INDEX `fk_shipments_status1_idx` (`status` ASC),
  CONSTRAINT `fk_shipments_status1`
    FOREIGN KEY (`status`)
    REFERENCES `deliver`.`status` (`status`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_shipments_warehouses1`
    FOREIGN KEY (`from_idwarehouses`)
    REFERENCES `deliver`.`warehouses` (`idwarehouses`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_shipments_warehouses2`
    FOREIGN KEY (`to_idwarehouses`)
    REFERENCES `deliver`.`warehouses` (`idwarehouses`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 24
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `deliver`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `deliver`.`users` (
  `idusers` INT(11) NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `role` TINYINT(1) NOT NULL DEFAULT 0,
  `country` VARCHAR(45) NOT NULL,
  `city` VARCHAR(45) NOT NULL,
  `street` VARCHAR(45) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  `is_deleted` TINYINT(1) NOT NULL DEFAULT 0,
  `created_on` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  PRIMARY KEY (`idusers`),
  INDEX `fk_users_countries1_idx` (`country` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 131
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `deliver`.`parcels`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `deliver`.`parcels` (
  `idparcels` INT(11) NOT NULL AUTO_INCREMENT,
  `default_idwarehouse` INT(11) NOT NULL,
  `weight` INT(11) NOT NULL,
  `category` VARCHAR(45) NOT NULL,
  `iduser` INT(11) NOT NULL,
  `to_warehouse` TINYINT(1) NOT NULL DEFAULT 1,
  `idshipments` INT(11) NULL DEFAULT NULL,
  `shipments_status` VARCHAR(45) NULL DEFAULT 'preparing',
  `is_deleted` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`idparcels`),
  INDEX `fk_parcels_users1_idx` (`iduser` ASC),
  INDEX `fk_parcels_warehouses1_idx` (`default_idwarehouse` ASC),
  INDEX `fk_parcels_categories1_idx` (`category` ASC),
  INDEX `fk_parcels_shipments1_idx` (`idshipments` ASC),
  CONSTRAINT `fk_parcels_categories1`
    FOREIGN KEY (`category`)
    REFERENCES `deliver`.`categories` (`category`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_parcels_shipments1`
    FOREIGN KEY (`idshipments`)
    REFERENCES `deliver`.`shipments` (`idshipments`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_parcels_users1`
    FOREIGN KEY (`iduser`)
    REFERENCES `deliver`.`users` (`idusers`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_parcels_warehouses1`
    FOREIGN KEY (`default_idwarehouse`)
    REFERENCES `deliver`.`warehouses` (`idwarehouses`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 27
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `deliver`.`tokens`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `deliver`.`tokens` (
  `token` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`token`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
