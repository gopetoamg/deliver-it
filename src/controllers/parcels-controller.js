import express from "express";
import parcelsErrors from "../errors/parcels-errors.js";
import shipmentsErrors from "../errors/shipments-errors.js";
import parcelsServices from "../services/parcels-services.js";
export const parcelsRouter = express.Router();

parcelsRouter

  /**
   * Get all parcels
   */
  .get("/", async (req, res) => {
    const { error, data } = await parcelsServices.getParcels();

    if (error === parcelsErrors.NO_PARCELS_FOUND) {
      return res.status(404).send({
        message: `No parcels found!`,
      });
    }

    res.status(200).send(data);
  })

  /**
   * Get parcel info by id
   */
  .get("/:idparcels", async (req, res) => {
    const { error, data } = await parcelsServices.getParcel(
      req.params.idparcels
    );

    if (error === parcelsErrors.PARCEL_NOT_FOUND) {
      return res.status(404).send({
        message: `No parcel with such id found!`,
      });
    }

    res.status(200).send(data);
  })

  /**
   * Get parcel by user id.
   */
  .get("/parcel_by_user/:id", async (req, res) => {
    const { error, data } = await parcelsServices.getParcelByUser(
      req.params.id
    );

    if (error === parcelsErrors.NO_PARCELS_FOR_USER_FOUND) {
      return res.status(404).send({
        message: `No parcel with such id found!`,
      });
    }

    res.status(200).send(data);
  })

  /**
   * Get parcels by category.
   */
  .get("/category/:category", async (req, res) => {
    const { error, data } = await parcelsServices.getParcelsFromCategory(
      req.params.category
    );

    if (error === parcelsErrors.PARCEL_NOT_FOUND) {
      //
      return res.status(404).send({
        message: `No parcels found in this category!`,
      });
    }

    res.status(200).send(data);
  })

  /**
   * Update parcel info by id
   */
  .put("/update/:id", async (req, res) => {
    const { error, data } = await parcelsServices.updateParcelInfo(
      req.params.id,
      req.body
    ); //

    if (error === parcelsErrors.NO_PARCELS_FOUND) {
      return res.status(404).send({
        message: `No parcel with such id!`,
      });
    }

    if (error === shipmentsErrors.SHIPMENT_FULL) {
      return res.status(400).send({
        message: shipmentsErrors.SHIPMENT_FULL,
      });
    }

    res.status(200).send(data);
  })

  /**
   * Edit is_deleted status. First param is the id of the parcel, second is the 1 or 0 is_deleted value.
   */
  .put("/delete/:id/:del", async (req, res) => {
    const { error, data } = await parcelsServices.deleteParcelById(
      req.params.id,
      req.params.del
    ); //

    if (error === parcelsErrors.NO_PARCELS_FOUND) {
      return res.status(404).send({
        message: `No parcel with such id!`,
      });
    }

    res.status(200).send(data);
  })

  /**
   * Get all parcels by weight.
   */
  .get("/weight/:weight", async (req, res) => {
    const { error, data } = await parcelsServices.getParcelsWithWeight(
      req.params.weight
    );

    if (error === parcelsErrors.NO_PARCELS_WITH_WEIGHT) {
      return res.status(404).send({
        message: `No parcel with such weight found!`,
      });
    }

    res.status(200).send(data);
  })

  /**
   * Get all parcels by their default warehouse values
   */
  .get("/warehouse/:warehouse", async (req, res) => {
    const { error, data } = await parcelsServices.getParcelsToDefaultWarehouse(
      req.params.warehouse
    );

    if (error === parcelsErrors.NO_PARCELS_TO_DEFAULT_WH) {
      return res.status(404).send({
        message: `No parcel with this default warehouse destination!`,
      });
    }

    res.status(200).send(data);
  })

  /**
   * Get all parcels from shipment with selected id value
   */
  .get("/shipment/:shipmentid", async (req, res) => {
    const { error, data } = await parcelsServices.getParcelsFromShipment(
      req.params.shipmentid
    );

    if (error === parcelsErrors.NO_PARCELS_FOR_SHIPMENT) {
      return res.status(404).send({
        message: `No parcels with such shipment!`,
      });
    }

    res.status(200).send(data);
  })

  /**
   * Filter all parcels by their deleted status
   */
  .get("/del/:del", async (req, res) => {
    const { error, data } = await parcelsServices.getParcelsByDelete(
      req.params.del
    );

    if (error === parcelsErrors.NO_PARCELS_FOUND) {
      return res.status(404).send({
        message: `No parcel with this default warehouse destination!`,
      });
    }

    res.status(200).send(data);
  })
  .put("/update/status/:idshipments", async (req, res) => {

    const { error, data } = await parcelsServices.updateParcelsShipmentStatus(
      req.params.idshipments
    ); //

    if (error === shipmentsErrors.SHIPMENT_NOT_FOUND) {
      return res.status(404).send({
        message: `No shipment with such id!`,
      });
    }

    res.status(200).send(data);
  })

  /**
   * Create new parcel
   */
  .post("/create", async (req, res) => {
    const { error, data } = await parcelsServices.newParcel(req.body);

    if (error === parcelsErrors.WRONG_PARCEL_INPUT) {
      return res.status(400).json({
        message: `No parcel data input`,
      });
    }

    res.status(201).send({
      message: `Parcel created`,
    });
  });
