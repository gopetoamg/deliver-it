import express from "express";
import usersErrors from "../errors/users-errors.js";
import userServices from "../services/users-services.js";
import createToken from "../auth/create_token.js";
// import { usedTokenCheck } from "../middlewares/validate-body.js";
import { authMiddleware } from "../auth/authMiddleware.js";
import { oldTokenCheck } from "../middlewares/used-token-check.js";

import { validateRequestSchema } from "../middlewares/validate-request-middleware.js";
import { registerValidatorSchema } from "../validators/register-validator-schema.js";
import { loginValidatorSchema } from "../validators/login-validator-schema.js";

// import validateBody from "../middlewares/validate-body.js";
// import registerUserValidator from "../validators/register-user-validator.js";

export const usersRouter = express.Router();

usersRouter
  /**
   * Get all users
   */
  .get("/", async (req, res) => {
    const { error, data } = await userServices.getUsers();

    if (error === usersErrors.USER_NOT_FOUND) {
      return res.status(404).send({
        message: `No users found!`,
      });
    }

    res.status(200).send(data);
  })
  /**
   * Get active users
   */
  .get("/active", async (req, res) => {
    const { error, data } = await userServices.getActiveUsers();

    if (error === usersErrors.USER_NOT_FOUND) {
      return res.status(404).send({
        message: `No users found!`,
      });
    }

    res.status(200).send(data);
  })
  /**
   * Get deleted users
   */
  .get("/deleted", async (req, res) => {
    const { error, data } = await userServices.getDeletedUsers();

    if (error === usersErrors.USER_NOT_FOUND) {
      return res.status(404).send({
        message: `No users found!`,
      });
    }

    res.status(200).send(data);
  })
  /**
   * Register new user
   */
  .post("/register", 
    validateRequestSchema(registerValidatorSchema),
    async (req, res) => {
    const { error, data } = await userServices.registerUser(req.body);

    if (error === usersErrors.ALREADY_EXISTS) {
      return res.status(409).json({
        message: `User with email: ${req.body.email} already exists!`,
      });
    }

    res.status(201);
  })
  /**
   * Login user. Returns a JWT token.
   */
  .post("/login", 
    validateRequestSchema(loginValidatorSchema),
    async (req, res) => {
    const { error, data } = await userServices.passCheck(req.body);

    if (error === usersErrors.USER_NOT_FOUND) {
      return res.status(401).send({errors: [{field: 'email', message: 'User has not been found!'}]});
    }
    if (error === usersErrors.INVALID_PASSWORD) {
      return res.status(403).send({errors: [{field: 'password', message: 'Invalid password'}]});
    }

    //CREATE THE TOKEN

    const token = createToken({
      idusers: data.idusers,
      email: data.email,
      role: data.role,
      firstName: data.first_name,
      lastName: data.last_name,
    });

    return res.status(200).send({ token });
  })
  /**
   * Logout user. Saves old token in the deprecated tokens table.
   */
  .post("/logout", authMiddleware, oldTokenCheck, async (req, res) => {
    const { error, data } = await userServices.saveToken(
      req.headers.authorization.replace("Bearer ", "")
    );
    

    // if (error === errors.PROBLEM) {
    //   return res.status(404).send({ message: `can't do your request now. Try latter` });
    // }

    return res.status(200).send({ message: "success logout" });
  })
  .put("/update/:id", async (req, res) => {
    const { error, data } = await userServices.updateUser(
      req.params.id,
      req.body
    );

    if (error === usersErrors.USER_NOT_FOUND) {
      return res.status(404).send({
        message: `User not found!`,
      });
    }
    res.send(data);
  });
