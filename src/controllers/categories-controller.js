import express from "express";
import categoriesServices from "../services/categories-services.js";

export const categoriesRouter = express.Router();

categoriesRouter
.get("/", async(req,res) => {
    const categories = await categoriesServices.getAllCategories();

    res.status(200).send(categories);
})