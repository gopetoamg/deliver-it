import categoriesData from "../data/categories-data.js";

const getAllCategories = async () => {
    return await categoriesData.getAllCategories();
}

export default {
    getAllCategories
};