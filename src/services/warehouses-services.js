import warehousesData from "../data/warehouses-data.js";
import warehouseData from "../data/warehouses-data.js";
import warehousesErrors from "../errors/warehouses-errors.js";
import { checkWarehouseExist } from "../data/warehouses-data.js";

/**
 * GET ALL WAREHOUSES IN CITY
 * @param {string} city
 * @returns
 */
const getAllWarehousesByCity = async (city) => {
  return await warehouseData.getAllWarehousesByCity(city);
};

/**
 * CREATE NEW wAREHOUSE
 * @param {string} country
 * @param {string} city
 * @param {string} street
 * @returns
 */
const createWarehouse = async (country, city, street) => {
  const exists = await checkWarehouseExist(country, city, street);

  if (exists) {
    return {
      error: warehousesErrors.WAREHOUSE_ALREADY_EXISTS,
    };
  } else {
    const newWarehouse = await warehouseData.createWarehouse(
      country,
      city,
      street
    );

    return {
      data: newWarehouse,
      error: null,
    };
  }
};

/**
 * GET ALL WAREHOUSES
 * @returns all warehouses
 */
const getAllWarehouses = async () => {
  return await warehouseData.getAllWarehouses();
};

/**
 * GET WAREHOUSE BY ID
 * @param {number} id
 * @returns
 */
const getSingleWarehouseById = async (id) => {
  const result = await warehousesData.getWarehouseById(id);

  if (result.length === 0) {
    return {
      error: warehousesErrors.WAREHOUSE_NOT_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: result,
  };
};

const updateWarehouse = async (idwarehouse, body) => {
  const updatedInfo = await warehousesData.updateWarehouseById(idwarehouse, body);
    
  if (!updatedInfo) {
    return {
      error: warehousesErrors.WAREHOUSE_NOT_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: updatedInfo,
  };
};
export default {
  getAllWarehousesByCity,
  createWarehouse,
  getAllWarehouses,
  getSingleWarehouseById,
  updateWarehouse
};
