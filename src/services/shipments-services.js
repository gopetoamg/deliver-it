import shipmentsData from "../data/shipments-data.js";
import shipmentsErrors from "../errors/shipments-errors.js";

/**
 * GET ALL SHIPMENTS
 * @returns object with the result or error
 */
const getShipments = async () => {
  const result = await shipmentsData.getAllShipments();

  if (result.length === 0) {
    return {
      error: shipmentsErrors.SHIPMENT_NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: result,
  };
};

/**
 * GET A SINGLE SHIPMENT BY ID
 * @param {number} id
 * @returns
 */
const getSingleShipment = async (id) => {
  const result = await shipmentsData.getShipmentById(id);

  if (result.length === 0) {
    return {
      error: shipmentsErrors.SHIPMENT_NOT_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: result,
  };
};

/**
 * GET SHIPMENTS FROM WAREHOUSE
 * @param {number} id
 * @returns
 */
const getShipmentByWarehouse = async (id) => {
  const result = await shipmentsData.getShipmentsFromWarehouse(id);

  if (result.length === 0) {
    return {
      error: shipmentsErrors.NO_SHIPMENT_FROM_WAREHOUSE,
      data: null,
    };
  }
  return {
    error: null,
    data: result,
  };
};

/**
 * GET SHIPMENTS TO WAREHOUSE
 * @param {number} id
 * @returns
 */
const getShipmentToWarehouse = async (id) => {
  const result = await shipmentsData.getShipmentsToWarehouse(id);

  if (result.length === 0) {
    return {
      error: shipmentsErrors.NO_SHIPMENT_TO_WAREHOUSE,
      data: null,
    };
  }
  return {
    error: null,
    data: result,
  };
};

/**
 * GET BY STATUS
 * @param {string} status
 * @returns
 */
const getShipmentByStatus = async (status) => {
  const result = await shipmentsData.getShipmentsByStatus(status);

  if (!result) {
    return {
      error: shipmentsErrors.SHIPMENT_STATUS_NOT_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: result,
  };
};

/**
 * UPDATE SHIPMENT BY ID
 * @param {number} idshipment
 * @param {object} body
 * @returns
 */
const updateShipment = async (idshipment, body) => {
  const updatedInfo = await shipmentsData.updateShipmentbyId(idshipment, body);

  if (!updatedInfo) {
    return {
      error: shipmentsErrors.SHIPMENT_NOT_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: updatedInfo,
  };
};

const newShipment = async (shipment) => {
  if (!shipment) {
    return {
      error: shipmentsErrors.NO_SHIPMENT_DATA,
      data: null,
    };
  }

  const createdShipment = await shipmentsData.createShipment(shipment);

  return {
    error: null,
    data: createdShipment
  };
};

const getShipmentStatus = async (id) => {
  const result = await shipmentsData.getShipmentStatusById(id);

  if (result.length === 0) {
    return {
      error: shipmentsErrors.SHIPMENT_NOT_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: result,
  };
};

export default {
  getShipments,
  getSingleShipment,
  getShipmentByWarehouse,
  getShipmentToWarehouse,
  getShipmentByStatus,
  updateShipment,
  newShipment,
  getShipmentByStatus,
  getShipmentStatus,
};
