import usersErrors from "../errors/users-errors.js";
import userRole from "../common/user-roles.js";
import bcrypt from "bcrypt";
import usersData, { updateUserById } from "../data/users-data.js";

/**
 * GET ALL USERS
 * @returns all users full info
 */
const getUsers = async () => {
  const result = await usersData.getAllUsers();

  if (result.length === 0) {
    return {
      error: usersErrors.USER_NOT_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: result,
  };
};

/**
 * GET ACTIVE USERS
 * @returns all active users full info
 */
const getActiveUsers = async () => {
  const result = await usersData.getAllActiveUsers();

  if (result.length === 0) {
    return {
      error: usersErrors.USER_NOT_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: result,
  };
};

/**
 * GET USE INFO BY EMAIL
 * @param {string} email
 */
const getUserByEmail = async (email) => {
  const result = await usersData.getSingleUserByEmail(email);

  

  if (!result) {
    return {
      error: usersErrors.USER_NOT_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: result,
  };

  // !result
  //   ? { error: usersErrors.USER_NOT_FOUND, data: null }
  //   : { error: null, data: result };
};

/**
 * GET ALL DELETED USERS
 * @returns all deleted users full info
 */
const getDeletedUsers = async () => {
  const result = await usersData.getAllDeletedUsers();

  if (result.length === 0) {
    return {
      error: usersErrors.USER_NOT_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: result,
  };
};

/**
 * REGISTER USER: registers new user with all the info
 * @param {object} user with the initial user data
 * @returns the created user
 */
const registerUser = async (user) => {
  const defaultRole = userRole.CUSTOMER;

  if (await usersData.checkIfUserExists(user.email)) {
    return {
      error: usersErrors.ALREADY_EXISTS,
      data: null,
    };
  }

  const createdUser = await usersData.registerNewUser(user, defaultRole);

  return {
    error: null,
    data: { createdUser },
  };
};

/**
 * PASSWORD CHECK: checks for correct pass and returns user's info
 * @param {object} body with the email and password of the user
 * @returns {object} with the data of the user
 */
const passCheck = async (body) => {
  if (!(await usersData.checkIfUserExists(body.email))) {
    return {
      error: usersErrors.USER_NOT_FOUND,
      data: null,
    };
  }

  const checkPass = await usersData.getSingleUserByEmail(body.email);

  if (await bcrypt.compare(body.password, checkPass.password)) {
    return {
      error: null,
      data: checkPass,
    };
  }

  return {
    error: usersErrors.INVALID_PASSWORD,
    data: null,
  };
};

/**
 * SAVE TOKEN IN BLACKLIST
 * @param {string} token 
 * @returns 
 */
const saveToken = async (token) => {

  const savedToken = await usersData.saveOldToken(token);

  if (!savedToken) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  return {
    error: null,
    data: savedToken,
  };
};

const updateUser = async (idUser, body) => {
  const updatedInfo = await updateUserById(idUser, body);
    
  if (!updatedInfo) {
    return {
      error: usersErrors.USER_NOT_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: updatedInfo,
  };
};

export default {
  getUsers,
  getUserByEmail,
  getActiveUsers,
  getDeletedUsers,
  registerUser,
  passCheck,
  saveToken,
  updateUser,
};
