export default {
    WAREHOUSE_NOT_FOUND: "Warehouse not found!",
    WAREHOUSE_ALREADY_EXISTS: 2,
    INVALID_WAREHOUSE: 3,
};
