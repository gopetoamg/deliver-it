export default {
    SHIPMENT_NOT_FOUND: "No shipments found!",
    ALREADY_EXISTS: "Shipment with this name already exists!",
    NO_SHIPMENT_FROM_WAREHOUSE: "No shipments from this warehouse!",
    NO_SHIPMENT_TO_WAREHOUSE: "No shipments to this warehouse!",
    NO_SHIPMENT_DATA: "No shipment data input",
    SHIPMENT_FULL: "Adding this parcel would the maximum shipment capacity",
};