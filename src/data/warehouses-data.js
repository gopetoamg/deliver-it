import pool from "../data/pool.js"

/**
 * GET WAREHOUSES BY CITY
 * @param {string} city 
 * @returns 
 */
const getAllWarehousesByCity = async (city) => {
    const sql = `
    SELECT *
    FROM warehouses
    WHERE city = '${city}'
    `;

    const result = await pool.query(sql);
    return result[0];
}

/**
 * GET ALL WAREHOUSES
 * @returns an array with the warehouses objects
 */
const getAllWarehouses = async () => {
    const sql = `
    SELECT * 
    FROM warehouses
    `;

    return await pool.query(sql);
}

/**
 * CREATE A WAREHOUSE
 * @param {string} country 
 * @param {string} city 
 * @param {string} street 
 * @returns 
 */
const createWarehouse = async (country,city,street) => {
    const sql = `
    INSERT INTO warehouses (country, city, street)
    VALUES (?,?,?)
    `;

    const result =  await pool.query(sql,[country,city,street]);

    return {
        idwarehouses: result.insertId,
        country: country,
        city: city, 
        street: street
    }
}

/**
 * GET WAREHOUSE INFO BY STREET
 * @param {string} street 
 * @returns 
 */
const getSingleWarehouseByStreet = async (street) => {
    const sql = `
    SELECT country, city , street
    FROM warehouses
    WHERE street = '${street}'
    `;

    const result = await pool.query(sql);
    return result[0];
}


/**
 * GET WAREHOUSE INFO BY ID
 * @param {number} id 
 * @returns 
 */
const getWarehouseById = async (id) => {

    const sql = `
        SELECT * 
        FROM warehouses
        WHERE idwarehouses = ?
        `;

    const result = await pool.query(sql, [id]);
    


    if (!result) {
        return null;
    };

    return result;
};

/**
 * UPDATE WAREHOUSE BY ID
 * @param {number} warehouseId 
 * @param {object} newData 
 * @returns 
 */
const updateWarehouseById = async (warehouseId, newData) => {
    const sqlQuery = `
        UPDATE warehouses  
        SET (?,?,?,?,?) 
        WHERE idwarehouses = '${warehouseId}'
        `;
    
    const result = await pool.query(sqlQuery, [
        newData.city,
        newData.country,
        newData.street,
        newData.is_deleted,
    ]);
    console.log(result);
    return result;
} 

/**
 * CHEK IF WAREHOUSE EXISTS
 * @param {string} country 
 * @param {string} city 
 * @param {string} street 
 * @returns 
 */
export const checkWarehouseExist = async (country, city, street) => {
    const sql = `
    SELECT country, city , street
    FROM warehouses
    WHERE country = '${country}'
    AND city = '${city}'
    AND street = '${street}'
    `;

    const result = await pool.query(sql);
    
    return result[0];
}

export default {
    getAllWarehousesByCity,
    createWarehouse,
    getSingleWarehouseByStreet,
    getAllWarehouses,
    getWarehouseById,
    updateWarehouseById,
}