import pool from "./pool.js";
import shipmentsData from "./shipments-data.js";

/**
 * GET ALL PARCELS FULL INFO
 * @returns all parcels
 */
const getAllParcels = async () => {
  const sql = `
    SELECT * 
    FROM parcels
    `;

  return await pool.query(sql);
};

/**
 * GET SINGLE PARCEL BY ID
 * @param {number} idparcels
 * @returns a single parcels full info
 */
const getSingleParcel = async (idparcels) => {
  const sql = `
    SELECT * 
    FROM parcels
    WHERE idparcels = ?
    `;

  return await pool.query(sql, [idparcels]);
};

// needs to be updated to search by email

/**
 * GET ALL PARCELS OF A SINGLE USER
 * @param {number} id
 * @returns all parcels of a single user and their full info
 */
const getParcelsByUserId = async (id) => {
  const sql = `
    SELECT *
    FROM parcels
    WHERE iduser = ?
    `;
  return await pool.query(sql, [id]);
};

/**
 * GET ALL PARCELS FROM A SINGLE CATEGORY
 * @param {string} category
 * @returns
 */
const getCategoryParcels = async (category) => {
  const sql = `
    SELECT * 
    FROM parcels
    WHERE category = ?
    `;

  const result = await pool.query(sql, [category]);

  if (result[0]) {
    return result[0];
  }

  return null;
};

/**
 * UPDATE PARCELS FULL INFO BY PARCEL ID
 * @param {number} idparcel
 * @param {object} newData
 * @returns an object with the updated posts info.
 */
const updateParcelInfo = async (idparcel, newData) => {
  
  const sqlQuery = `
    UPDATE parcels 
    SET default_idwarehouse = ?, weight = ?, category = ?, iduser = ?, to_warehouse = ?, idshipments = ?, shipments_status = ?
    WHERE idparcels = ?`;

  return await pool.query(sqlQuery, [
    newData.default_idwarehouse,
    newData.weight,
    newData.category,
    newData.iduser,
    newData.to_warehouse,
    newData.idshipments,
    newData.shipment_status,
    idparcel,
  ]);

  const result = await getSingleParcel(idparcel);

  if (result[0]) {
    return result[0];
  }
  return null;
};

/**
 * DELETE PARCELS BY  ID
 * @param {number} idparcel
 * @param {object} newData
 * @returns an object with the updated posts info.
 */
const deleteParcel = async (idparcel, del) => {
  const sqlQuery = `UPDATE parcels SET is_deleted = ? WHERE idparcels = ?`;

  const updatedUser = await pool.query(sqlQuery, [del, idparcel]);

  const result = await getSingleParcel(idparcel);

  if (result[0]) {
    return result[0];
  }
  return null;
};

/**
 * GET PARCELS WITH WEIGHT
 * @param {number} weight
 * @returns all parcels with the selected weight.
 */
const getParcelsByWeight = async (weight) => {
  const sql = `
    SELECT * 
    FROM parcels
    WHERE weight = ?
    `;

  const result = await pool.query(sql, [weight]);

  if (result[0]) {
    return result[0];
  }

  return null;
};

/**
 * GET PARCELS TO WAREHOUSE
 * @param {number} warehouse
 * @returns all parcels with the selected warehouse.
 */
const getParcelsByDefaultWarehouse = async (warehouse) => {
  const sql = `
    SELECT * 
    FROM parcels
    WHERE default_idwarehouse = ?
    `;

  const result = await pool.query(sql, [warehouse]);

  if (result[0]) {
    return result[0];
  }

  return null;
};

/**
 * GET PARCELS FROM SHIPMENT
 * @param {number} idshipment
 * @returns all parcels with the selected weight.
 */
const getParcelsByShipment = async (idshipment) => {
  const sql = `
      SELECT * 
      FROM parcels
      WHERE idshipments = ?
      `;

  const result = await pool.query(sql, [idshipment]);

  if (result[0]) {
    return result[0];
  }

  return null;
};

/**
 * FILTER PARCELS BY IS_DELETED
 * @param {number} del
 * @returns all parcels with the selected is_deleted value
 */
const getParcelsByDel = async (del) => {
  const sql = `
      SELECT * 
      FROM parcels
      WHERE is_deleted = ?
      `;

  const result = await pool.query(sql, [del]);

  if (result[0]) {
    return result[0];
  }

  return null;
};

const createParcel = async (parcel) => {
  const sql = `
    INSERT INTO parcels (default_idwarehouse, weight, category, iduser)
    VALUES (?, ?, ?, ?)
    `;
  return await pool.query(sql, [
    parcel.default_idwarehouse,
    parcel.weight,
    parcel.category,
    parcel.iduser,
  ]);
};

/**
 * UPDATE PARCELS STATUS BY SHIPMENT ID
 * @param {number} idshipments
 * @returns
 */

const updateParcelsStatus = async (idshipments) => {
  const shipmentStatus = await shipmentsData.getShipmentStatusById(idshipments);

  const sqlQuery = `
    UPDATE parcels 
    SET shipments_status=?
    WHERE idshipments=?`;

  const updatedParcelsStatus = await pool.query(sqlQuery, [
    shipmentStatus,
    idshipments,
  ]);

  //Error check here

  return {
    message: "Parcels status updated",
  };
};

export default {
  getSingleParcel,
  getAllParcels,
  getParcelsByUserId,
  getCategoryParcels,
  updateParcelInfo,
  deleteParcel,
  getParcelsByWeight,
  getParcelsByDefaultWarehouse,
  getParcelsByShipment,
  getParcelsByDel,
  createParcel,
  updateParcelsStatus,
};
