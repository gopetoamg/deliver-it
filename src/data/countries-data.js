import pool from "./pool.js";

/**
 * CHECK IF COUNTRY EXISTS
 * @param {string} name
 * @returns boolean
 */
const checkCountryExists = async (name) => {
  return !!(await getSingleCountryByName(name));
};

/**
 * GET ALL COUNTRIES
 * @returns all countries
 */
const getAllCountries = async () => {
  const sql = `
    SELECT * 
    FROM countries
    `;

  return await pool.query(sql);
};

/**
 * GET ALL ACTIVE COUNTRIES
 * @returns all non deleted countries
 */
const getAllActiveCountries = async () => {
  const sql = `
    SELECT * 
    FROM countries
    WHERE is_deleted = 0
    `;

  return await pool.query(sql);
};

/**
 * GET COUNTRY BY NAME
 * @param {string} name
 * @returns the selected country
 */
const getSingleCountryByName = async (name) => {
  const sql = `
    SELECT *
    FROM countries
    WHERE country = ? AND is_deleted = 0
    `;

  const result = await pool.query(sql, [name]);

  if (result[0]) {
    return result[0];
  }

  return null;
};

/**
 * ADD NEW COUNTRY
 * @param {string} country
 * @returns a success message.
 */
const createNewCountry = async (country) => {
  const sql = `
    INSERT INTO countries (country)
    VALUES (?)
    `;

  return await pool.query(sql, [country]);
};

export default {
  getAllCountries,
  getAllActiveCountries,
  createNewCountry,
  getSingleCountryByName,
  checkCountryExists,
};
