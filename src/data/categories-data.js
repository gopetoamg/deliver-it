import pool from "./pool.js";

const getAllCategories = async () => {
    const sql = `
    SELECT * 
    FROM categories
    `;

    return await pool.query(sql);
}


export default {
    getAllCategories
};